const express = require('express');
const config = require('./config/config');
const glob = require('glob');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);


var fs = require('fs'),
    http = require('http'),
    https = require('https');

var options = {
    key: fs.readFileSync('./ssl/key.pem'),
    cert: fs.readFileSync('./ssl/certificate.pem'),
};


mongoose.connect(config.db, {useMongoClient: true});
const db = mongoose.connection;
db.on('error', () => {
  throw new Error('unable to connect to database at ' + config.db);
});

const models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});
const app = express();

app.use(session({
  saveUninitialized: false,
  resave: false,
  secret: 'MeepoDrowRangerSheepStick1292188712',
  store: new MongoStore({mongooseConnection: db })
}));

module.exports = require('./config/express')(app, config);

https.createServer(options, app).listen(config.port, function(){
  console.log("Express server listening on port " + config.port);
});

