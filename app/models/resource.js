const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ResourceSchema = new Schema({
  title: String,
  info: String,
  source: { type: Schema.Types.ObjectId, ref: 'User' },
  listing_type: String,
  loc: {
    type: { type: Schema.Types.Point },
    coordinates: []
  },
  printable: Boolean,
  rating: Number,
  photo: String
});

ResourceSchema.virtual('date')
  .get(() => this._id.getTimestamp());

mongoose.model('Resource', ResourceSchema);

