const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Article = mongoose.model('Article');

module.exports = (app) => {
  app.use('/', router);
};

router.get('/', (req, res, next) => {
  if (req.session) {

  }
  Article.find((err, articles) => {
    if (err) return next(err);
    res.render('index', {
      title: 'GirlsInTech',
      articles: articles
    });
  });
});


router.get('/discover', (req, res, next) => {
  res.render('map', {
    title: 'GirlsInTech'
  });
});
