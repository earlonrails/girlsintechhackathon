const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Resource = mongoose.model('Resource');

module.exports = (app) => {
  app.use('/resources', router);
};


// Create Resource
router.post('/', (req, res, next) => {
  const resource = new Resource(req.body);
  resource.save((err, source) => {
    if (err) {
      console.log(err);
      console.log(source);
    }
    res.json({
      resource: source
    });
  });
});


// Get Resource By location
router.get('/nearby', (req, res, next) => {
  var lat = parseFloat(req.query.lat);
  var long = parseFloat(req.query.long);
  var getCallback = function(err, results, stats) {
    if (err) {
      console.log(err);
    }
    return res.json({
      resource: results
    });
  };

  var geoNearQuery = [
    { "$geoNear": {
        "near": {
          "type": "Point",
          "coordinates": [long, lat]
        },
        "distanceField": "distance",
        "spherical": true,
        "maxDistance": 600000
    }}
  ]

  Resource.aggregate(geoNearQuery, getCallback);
});


router.get('/:id', (req, res, next) => {
  Resource.findById(req.params.id, (err, source) => {
    if (err) return res.status(500).send(err);

    return res.json({
      resource: source
    });
  });
});
