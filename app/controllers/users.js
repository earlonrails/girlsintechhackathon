const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = (app) => {
  app.use('/users', router);
};

router.post('/', (req, res, next) => {
  const user = new User(req.body);
  user.save((err, user) => {
    if (err) {
    }
    req.session.userID = user._id.toString();
    res.json({
      user: user
    });
  });
});
