$(document).ready(function() {
  console.log( "ready!" )
  var map = L.map('map').setView([51.505, -0.09], 13)
  var filterButton = $('#searchbox-menubutton');
  var filterDiv = $('filterPage');
  var mapDiv = $('#map');
  var accessToken = 'pk.eyJ1Ijoid2FsbHN1cmZlciIsImEiOiJjams2MnZncTYwdDR0M2treW91NmFmNGVvIn0.haZAGgAWlwlrLLKNJa3NAg';


  var detailsDiv = $('#details');
  var detailImg = $('#detail-img');
  var detailType = $('#detail-type');
  var detailTitle = $('#detail-title');
  var detailAddress = $('#detail-address');

  L.AwesomeMarkers.Icon.prototype.options.prefix = 'fa';

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + accessToken, {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'your.mapbox.access.token'
  }).addTo(map)
  map.locate({setView: true, maxZoom: 16})


  // coordinates
  // :
  // (2) [-122.396719, 37.795507]
  // distance
  // :
  // 715.8364974000847
  // icon
  // :
  // e {options: {…}, _initHooksCalled: true, markerColor: "red"}
  // info
  // :
  // ""
  // listing_type
  // :
  // "Restroom"
  // phone
  // :
  // null
  // title
  // :
  // null
  // _id
  // :
  // "5b5dd9939698932eb2474c9e"

  var showMarkerDetails = function(attrs) {
    console.log(attrs)
    attrs.icon.markerColor = 'red'
    detailsDiv.show()
    detailType.text(attrs.listing_type)
    detailImg.attr('src', attrs.photo)
    detailTitle.text(attrs.title)
    detailAddress.text(attrs.address)

  }

  var pickIcon = function(type) {
    switch(type) {
        case 'Shelter':
            return 'home';
        case 'Legal':
            return 'balance-scale';
        case 'Medical':
            return 'user-md';
        case 'Jobs':
            return 'briefcase';
        case 'Dependency':
            return 'pills';
        case 'Restroom':
            return 'bath';
        case 'Food':
            return 'utensils';
        default:
            return 'globe';
    }
  }

  var colors = ['red', 'blue', 'green', 'purple', 'orange', 'darkred', 'lightred', 'beige', 'darkblue', 'darkgreen', 'cadetblue', 'darkpurple', 'white', 'pink', 'lightblue', 'lightgreen', 'gray', 'black', 'lightgray'];

  var pickColor = function(type) {
    switch(type) {
        case 'Shelter':
            return 'purple';
        case 'Legal':
            return 'gray';
        case 'Medical':
            return 'red';
        case 'Jobs':
            return 'beige';
        case 'Dependency':
            return 'green';
        case 'Restroom':
            return 'orange';
        case 'Food':
            return 'lightred';
        default:
            return 'blue';
    }
  }


  var onLocationFound = function(e) {
    var radius = e.accuracy * 2;

    $.get('/resources/nearby', {lat: e.latlng.lat, long: e.latlng.lng}, function(data, status) {
      data.resource.forEach(function(ele) {
        var awesomeMarker = L.AwesomeMarkers.icon({
           icon: pickIcon(ele.listing_type),
           markerColor: pickColor(ele.listing_type)
         });
        ele['icon'] = awesomeMarker
        var marker = L.marker(L.latLng(ele.coordinates[1], ele.coordinates[0]), {icon: awesomeMarker})
        marker.on('click', function() {
          showMarkerDetails(ele)
        })
        marker.addTo(map)
      })
    })
  }

  map.on('locationfound', onLocationFound);

  filterButton.click(function(ele) {
    mapDiv.toggle();
    filterDiv.toggle();
  })
});

