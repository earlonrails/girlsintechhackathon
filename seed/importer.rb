# gem install csv
# gem install mongo

require 'csv'
require 'mongo'


client = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'app-development')
db = client.database
resources = client[:resources]

CSV.foreach("shelters.csv", :headers => true) do |row|
  title = row[0]
  address = row[1]
  phone = row[2]
  lat = row[3].to_f
  lng = row[4].to_f
  type = row[5]
  img = row[6]

  resource_hash = {
    title: title,
    info: "",
    phone: phone,
    listing_type: type,
    address: address,
    photo: img,
    coordinates: [lng, lat]
  }

  existing_resource = resources.find({ title: title }).first rescue nil

  if existing_resource
    puts "Already have record with name #{title}"
  else
    result = resources.insert_one(resource_hash)
    result.n
  end
end

